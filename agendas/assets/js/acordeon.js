/***********************************************
* Switch Menu script- by Martial B of http://getElementById.com/
* Modified by Dynamic Drive for format & NS4/IE4 compatibility
* Visit http://www.dynamicdrive.com/ for full source code
***********************************************/

if (document.getElementById){ //DynamicDrive.com change
	document.write('<style type="text/css">\n')
	document.write('.submenu{display: none;}\n')
	document.write('</style>\n')
}

function SwitchMenu(obj,ref){
	if(document.getElementById){

	var el = document.getElementById(obj);
	var divs = document.getElementById("masterdiv").getElementsByTagName("div")
	var ar = document.getElementById("masterdiv").getElementsByTagName("span"); //DynamicDrive.com change

		for (var i=0; i<divs.length; i++){
			if (divs[i].id == "ico") {
					divs[i].innerHTML = "+";
			}
		}

		if(el.style.display != "block"){ //DynamicDrive.com change
			for (var i=0; i<ar.length; i++){
				if (ar[i].className=="submenu") //DynamicDrive.com change
				ar[i].style.display = "none";
			}
			el.style.display = "block";			
			ref.childNodes[0].innerHTML = "-";

		} else {
			el.style.display = "none";
			ref.childNodes[0].innerHTML = "+";
		}
		
		ScrollingToTop(el,0);
	}
}

/*	Developed by Gushand Corporation  - All rtights reserved */
function ScrollingToTop(element,offSet)
{
	if (offSet >= 100) 
		return;
		
	var	dist = element.offsetTop - offSet;
	scrollTo(0,dist);
 
	setTimeout(function() {
        ScrollingToTop(element,offSet+1);
    }, 1);
}
