
/* quant é a quantidade de páginas que vamos abrir! */

/* 
var quant = 14;
var diretorio = "paginas";


essas variáveis serão definidas na agenda que chama o container.js
*/

var qual;

$(document).ready(function() {
	$("#conteudo").hide();
	$("#menuItens").fadeToggle(1);
	if($.cookie('guardar')==undefined) {
		qual = 1;
		$.cookie('guardar', qual);
	} else {
		qual = $.cookie('guardar');
	}
	//$.removeCookie("guardar");
	
	$(document).keydown(function(e){
		if (e.keyCode == 39) { 
			//alert("direita");
			var op = qual;
			op++;
			if(qual<quant) {
				Clicou(op);
				AbrirTela(this,"assets/"+diretorio+"/pagina_" + op + ".html");
			} else {
				$("#sombraAvancar").html("");
			}	
		}
		if (e.keyCode == 37) { 
			//alert("esquerda");
			var op = qual;
			op--;
			if(qual>=2) {
				Clicou(op);
				AbrirTela(this,"assets/"+diretorio+"/pagina_" + op + ".html");
			} else {
				$("#sombraVoltar").html("");
			}
		}
	});

});

function Inicia() {
	Clicou(qual);
	AbrirTela(this, "assets/"+diretorio+"/pagina_" + qual + ".html");
}

function Menu() {
	$("#paginacao").html("");
	$("#paginacao").append("<span class='pagNumClick'><p><img src='assets/template/seta_menu.png' alt='À direita está disponível o menu de navegação! Você está na página "+qual+".'/></p></span>");
	
	for (i=1; i<=quant; i++) {
		if (i==qual){
			$("#paginacao").append("<span class='pagNumClick'><p><img src='assets/template/pag_"+qual+".png' alt='você está na página "+qual+".'/></p></span>" + " ");
		}else{
			$("#paginacao").append("<a href='#' onClick='Clicou(" + i + "); AbrirTela(this,\"assets/"+diretorio+"/pagina_" + i + ".html\");' class='pagNum'><p><img src='assets/template/link_"+i+".png' alt='acesso à página "+i+".'/></p></a>" + " ");
		}
	}
}

function Clicou(op) {
	qual = op;
	ctrlDiv('menuItens', false);
	$.cookie('guardar', qual);
	Menu();
	navAvancar(qual);
	navVoltar(qual);
}

function navVoltar(op) {
	op--;
	if(qual>=2) {
		$("#sombraVoltar").html("<a alt='teste de acesso ao nvda.' href='javascript://' onclick='Clicou(" + op + "); AbrirTela(this,\"assets/"+diretorio+"/pagina_" + op + ".html\");''><div class='nav btVoltar '>Botão Voltar.</div></a>");
	} else {
		$("#sombraVoltar").html("");
	}
}

function navAvancar(op) {
	op++;
	if(qual<quant) {
		$("#sombraAvancar").html("<a href='javascript://' onclick='Clicou(" + op + "); AbrirTela(this,\"assets/"+diretorio+"/pagina_" + op + ".html\");''><div class='nav btAvancar '>Botão Avançar.</div></a>");
	} else {
		$("#sombraAvancar").html("");
	}	
}

function AbrirTela(ref, url){
	$("#conteudo").html("<iframe src='" + url + "' class='iframe' frameborder='0' scrolling='auto'></iframe>");
	$("#conteudo").fadeIn(1000,null,null).show();
	return false;
}

function FecharTela(){
	$("#conteudo:visible").removeAttr("style").fadeOut();
	return false;
}

function ctrlDiv(divid,prop) { 
	jQuery(document).ready(function() {
		if (prop) {
			$("#"+divid).fadeToggle(1000);
		} else {
			$("#"+divid).fadeOut(1000);
		}
	});
}
